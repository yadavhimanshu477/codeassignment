const userService = require('./userService');
const userServiceObj = new userService()
const { validationResult } = require('express-validator');

const UserController =  {
    register: async function (req, res) {
        const errors = validationResult(req);
        console.log(errors)
        if (!errors.isEmpty()) {
            return res.status(422).json({ message: 'INVALID OR MISSED PARAMETERS', errors: errors.array() });
        }
        const data = req.body
        const registerData = await userServiceObj.register(data)

        if (registerData.err) {
            res.status(500).json({message: 'INTERNAL SERVER ERROR'});
        } else {
            res.status(200).json({ message:'REGISTERED SUCCESSFULLY', data: registerData.user });
        }
    },

    login: async function (req, res) {
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
            return res.status(422).json({ message: 'INVALID PARAMETERS', errors: errors.array() });
        }
        const data = req.body
        const loginData = await userServiceObj.login(data)

        if (loginData.err) {
            res.status(500).json({message: 'INTERNAL SERVER ERROR'});
        } else if (loginData.response) {
            res.status(200).json({ message:'LOGIN SUCCESSFULL!', data: loginData.response });
        } else {
            res.status(401).json({ message:'UNAUTHORISED!' });
        }
    },

    validateToken: async function (req, res) {
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
            return res.status(422).json({ message: 'INVALID PARAMETERS', errors: errors.array() });
        }

        const data = req.body
        const tokenData = await userServiceObj.validateToken(data)

        if (tokenData.err) {
            res.status(500).json({message: 'INTERNAL SERVER ERROR'});
        } else if (tokenData.response) {
            res.status(200).json({ message:'TOKEN REFRESHED!', data: tokenData.response });
        } else {
            res.status(404).json({ message:'INVALID REFRESH TOKEN!' });
        }
    }

}

module.exports = UserController;
