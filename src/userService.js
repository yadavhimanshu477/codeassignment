const userModel = require('./models/userModel');
const jwt = require('jsonwebtoken')
const config = require('../config/config');
const tokenList = {}

class UserService {
    async register (data) {
        try {
            const userData = { 
                username: data.username , 
                password: data.password, 
                email: data.email, 
                address: data.address 
            };
            const model = new userModel(userData);
            const user = await model.save();

            return { err: null, user };

        } catch (err) {
            return { err };
        }
    }

    async login (data) {
        try {
            const queryUser = { 
                username: data.username, 
                password: data.password 
            }
            const projection = {_id: 1}
            const userData = await userModel.findOne(queryUser, projection)

            if (!userData) {
                return { err: null, response: null };
            } else {
                const token = jwt.sign(queryUser, config.secret, { expiresIn: config.tokenLife})
                const refreshToken = jwt.sign(queryUser, config.refreshTokenSecret, { expiresIn: config.refreshTokenLife})
                const response = {
                    "token": token,
                    "refreshToken": refreshToken,
                    "expiresIn": config.tokenLife/(1000*60)+ ' minutes'
                }
                tokenList[refreshToken] = response
                return { err: null, response };
            }
            
        } catch (err) {
            return { err };
        }
    }

    async validateToken (data) {
        try {
            if((data.refreshToken) && (data.refreshToken in tokenList)) {
                const user = {
                    username: data.username,
                    password: data.password
                }
                const token = jwt.sign(user, config.secret, { expiresIn: config.tokenLife})
                const response = tokenList[data.refreshToken]
                response.expiresIn = response.expiresIn/(1000*60)
                // update the token in the list
                response.token = token
                return { err: null, response }; 
            } else {
                return { err: null, response: null }; 
            }
        } catch (err) {
            return { err };
        }
    }
}

module.exports = UserService;
