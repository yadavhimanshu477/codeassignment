const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const schema = new Schema({
    username     : { type: String },
    email        : { type: String },
    password     : { type: String },
    address      : { type: String }
}, {
    timestamps: { createdAt: 'created_at' }
});


const User = mongoose.model('user', schema);

module.exports = User;