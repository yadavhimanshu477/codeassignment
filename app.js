const express = require('express')
const router = express.Router()
const bodyParser = require('body-parser')
const logger = require('morgan')
const port = 3000
const mongoose = require('mongoose');
const app = module.exports = express()
app.use(logger('dev'))
app.use(bodyParser.urlencoded({ extended: true }))

// require('./Connections/MongooseConnection')
require('./appRoutes.js')(router)
app.use(router)

// If no route is matched by now, it must be a 404
app.use(function (req, res, next) {
  var err = new Error('Not Found')
  err.status = 404
  next(err)
})

const dbUrl = 'mongodb://localhost:27017/codeAssignment';
mongoose.connect(dbUrl ,{ useNewUrlParser: true, useUnifiedTopology: true } ,(err) => {
  if (err) {
    console.log('err', err)
  } else {
    console.log('mongodb connected');
  }
})

// Start the server
app.set('port', port)
app.listen(app.get('port'))

console.log(`process started`);
