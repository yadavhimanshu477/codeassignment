### Installation

It requires [Node.js](https://nodejs.org/) v4+ to run.

Clone repository, Install the dependencies and start the server.

```sh
$ git clone https://yadavhimanshu477@bitbucket.org/yadavhimanshu477/codeassignment.git
$ cd codeassignment
$ npm install
$ npm start
```

### Api Description

These are the api path:

```sh
/register (POST):
Request Body: [username, password, email, address]
check('username').isString(),
check('password').isLength({ min: 5 }),
check('email').isString(),
check('address').isString()
```

```sh
/login (POST):
Request Body: [username, password]
check('username').isEmail(),
check('password').isLength({ min: 5 })
```


```sh
/validateToken (POST):
Request Body: [username, refreshToken]
check('username').isEmail(),
check('refreshToken').isString()
```

### Deployment

I have deployed the app to my aws instance, Anyone can check from here:

```sh
http://13.234.110.145:3000/register
http://13.234.110.145:3000/login
http://13.234.110.145:3000/validateToken
```

