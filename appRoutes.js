
const userController = require('./src/usersController')
const { check } = require('express-validator');

const registerUser = [
  check('username').isString().isLength({ min: 3 }),
  check('password').isLength({ min: 5 }),
  check('email').isString(),
  check('address').isString()
]

const validateUser = [
  check('username').isString().isLength({ min: 3 }),
  check('password').isLength({ min: 5 })
]

const validateTokenData = [
  check('username').isString().isLength({ min: 3 }),
  check('password').isLength({ min: 5 }),
  check('refreshToken').isString()
]

module.exports = function (router) {

  router.post('/register', registerUser, userController.register);
  router.post('/login', validateUser, userController.login);
  router.post('/validateToken', validateTokenData, userController.validateToken);

}
